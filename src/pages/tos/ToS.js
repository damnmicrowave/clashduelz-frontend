import React from 'react'
import './styles.scss'

export const ToS = ({ texts }) => (
  <div className="tos container">
    <h3>{texts.title}</h3>
    <div className="content" dangerouslySetInnerHTML={{ __html: texts.contents }}/>
  </div>
)
