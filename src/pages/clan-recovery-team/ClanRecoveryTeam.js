import React from 'react'
import { apiRequest } from '../../utils'
import { Loading } from '../../includes'

import './styles.scss'

export class ClanRecoveryTeam extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      loaded: false,
      team: []
    }
  }

  componentDidMount() {
    apiRequest(({ team }) => {
      this.setState({
        loaded: true,
        team
      })
    }, 'recovery_team')
  }

  render() {
    const {
      title,
      description,
      usernameColumn
    } = this.props.texts
    const {
      loaded,
      team
    } = this.state
    return (
      <>
        {!loaded && <Loading/>}
        <div className="recovery container">
          <h3>
            {title}
          </h3>
          <div className="desc">{description}</div>
          {loaded &&
          <div className="board">
            <div className="row">
              <div className="number">№</div>
              <div className="username">{usernameColumn}</div>
            </div>
            <div className="divider"/>
            {team.map(({ nickname, name, role }, index) => (
              <React.Fragment key={index}>
                <div className="row">
                  <div className="number">{index + 1}</div>
                  <div className="username">
                    {nickname} {name && <>({name})</>}<span>{this.props.texts[role]}</span>
                  </div>
                </div>
                <div className="divider"/>
              </React.Fragment>
            ))}
          </div>}
        </div>
      </>
    )
  }
}