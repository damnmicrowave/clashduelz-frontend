import React from 'react'
import { apiRequest } from '../../utils'
import { Redirect } from 'react-router'
import { InputField } from '../../includes'

import './styles.scss'

import exitSVG from './media/logout.svg'
import listSVG from './media/list.svg'
import arrowSVG from './media/arrow.svg'
import zCoin from './media/zCoin.svg'
import Reaptcha from 'reaptcha'

export class Profile extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      inviteLink: '',

      refCode: '',

      oldPassword: '',
      newPassword1: '',
      newPassword2: '',

      showHistory: false,
      duels: false
    }

    this.recaptchaLink = React.createRef()
    this.recaptchaPassword = React.createRef()
    this.recaptchaRefCode = React.createRef()
    this.recaptchaClaimIncome = React.createRef()
  }

  componentDidMount() {
    const {
      showPopup,
      texts,
      loggedIn
    } = this.props
    const { errorPopup } = texts
    if ( loggedIn ) {
      apiRequest(({ status, duels }) => {
        if ( status === 'error' ) {
          showPopup(errorPopup)
        } else {
          this.setState({ duels: duels })
        }
      }, 'get_duels')
    }
  }

  logout() {
    const { setLoading } = this.props
    setLoading(true)
    apiRequest(() => {
      window.location.reload()
    }, 'logout')
  }

  changePassword(token) {
    const {
      setLoading,
      showPopup,
      texts
    } = this.props
    const {
      incorrectPasswordPopup,
      passwordsDifferPopup,
      shortPasswordPopup,
      passwordChangedPopup,
      captchaFailedPopup,
      errorPopup
    } = texts
    setLoading(true)
    apiRequest(({ error, captchaError, passwordIcorrect, passwordsMismatch, passwordTooShort }) => {
      setLoading(false)
      if ( captchaError ) {
        showPopup(captchaFailedPopup)
      } else if ( passwordIcorrect ) {
        showPopup(incorrectPasswordPopup)
      } else if ( passwordsMismatch ) {
        showPopup(passwordsDifferPopup)
      } else if ( passwordTooShort ) {
        showPopup(shortPasswordPopup)
      } else if ( error ) {
        showPopup(errorPopup)
      } else {
        showPopup(passwordChangedPopup)
      }
    }, 'change_password', {
      method: 'POST',
      data: {
        ...this.state,
        token: token
      }
    })
  }

  changeLink(token) {
    const {
      setLoading,
      showPopup,
      texts
    } = this.props
    const {
      invalidLinkPopup,
      foreignLinkPopup,
      identicalLinkPopup,
      linkUpdatedPopup,
      captchaFailedPopup,
      errorPopup
    } = texts
    setLoading(true)
    apiRequest(({ error, captchaError, invalidLink, tagsMismatch, linksIdentical }) => {
      setLoading(false)
      if ( captchaError ) {
        showPopup(captchaFailedPopup)
      } else if ( invalidLink ) {
        showPopup(invalidLinkPopup)
      } else if ( tagsMismatch ) {
        showPopup(foreignLinkPopup)
      } else if ( linksIdentical ) {
        showPopup(identicalLinkPopup)
      } else if ( error ) {
        showPopup(errorPopup)
      } else {
        showPopup(linkUpdatedPopup)
      }
    }, 'change_invite_link', {
      method: 'POST',
      data: {
        ...this.state,
        token: token
      }
    })
  }

  applyCode(token) {
    const {
      setLoading,
      showPopup,
      updateProfile,
      texts
    } = this.props
    const {
      notAuthenticatedPopup,
      incorrectCodePopup,
      alreadyAppliedPopup,
      selfInvitePopup,
      captchaFailedPopup,
      successfullyAppliedPopup
    } = texts
    setLoading(true)
    apiRequest(({ status, captchaError, reason, profile }) => {
      setLoading(false)
      if ( captchaError ) {
        showPopup(captchaFailedPopup)
      } else if ( status === 'error' ) {
        const reasons = {
          notAuthenticated: notAuthenticatedPopup,
          incorrectCode: incorrectCodePopup,
          alreadyApplied: alreadyAppliedPopup,
          selfInvite: selfInvitePopup
        }
        showPopup(reasons[reason])
      } else {
        showPopup(successfullyAppliedPopup)
        updateProfile(profile)
      }
    }, 'apply_code', {
      method: 'POST',
      data: {
        ...this.state,
        token: token
      }
    })
  }


  claimIncome(token) {
    const {
      setLoading,
      showPopup,
      updateProfile,
      texts
    } = this.props
    const {
      notAuthenticatedPopup,
      lowAmountPopup,
      captchaFailedPopup
    } = texts
    setLoading(true)
    apiRequest(({ status, captchaError, reason, profile }) => {
      setLoading(false)
      if ( captchaError ) {
        showPopup(captchaFailedPopup)
      } else if ( status === 'error' ) {
        const reasons = {
          notAuthenticated: notAuthenticatedPopup,
          lowAmount: lowAmountPopup
        }
        showPopup(reasons[reason])
      } else {
        updateProfile(profile)
      }
    }, 'claim_income', {
      method: 'POST',
      data: { token: token }
    })
  }

  validate() {
    const {
      newPassword1,
      newPassword2
    } = this.state
    return newPassword1 === newPassword2 && newPassword1
  }

  validateRef() {
    return this.state.refCode.length >= 4
  }

  validateIncome() {
    return this.props.profile['ref_earnings'] >= 1
  }

  render() {
    const {
      loggedIn,
      profile,
      texts
    } = this.props
    if ( !loggedIn ) return <Redirect to="/login/"/>
    const {
      inviteLink,
      refCode,
      oldPassword,
      newPassword1,
      newPassword2,
      showHistory,
      duels
    } = this.state
    const {
      username,
      tag,
      total_wins,
      total_winnings,
      'invited_by__username': invitedBy,
      refs,
      'ref_earnings': refEarnings
    } = profile
    const {
      activityLog,
      you,
      infoLeft,
      infoRight,
      linkTitle,
      linkPlaceholder,
      linkButton,
      passwordTitle,
      oldPasswordPlaceholder,
      newPasswordPlaceholder,
      confirmPasswordPlaceholder,
      passwordButton,
      refCodeTitle,
      appliedMessage,
      invitedBy: invitedByText,
      codePlaceholder,
      applyButton,
      referralInfoTitle,
      linkLabel,
      totalReferralsLabel,
      referralIncomeLabel,
      claimButton
    } = texts
    return (
      <div className="profile container">
        {showHistory ? <>
          <h3>
            {activityLog}
            <img key={12} onClick={() => {
              this.setState({ showHistory: false })
            }} style={{ height: '1.5rem' }} src={arrowSVG} alt="back"/>
          </h3>
          {duels && <div className="duels-list">
            {duels.map(({ win, reward, opponentUsername, opponentTag }, index) => (
              <div key={index} className={`duel-log${win ? ' win' : ''}`}>
                <div className="you">
                  {you}
                </div>
                <div className="vs">
                  <span>V</span>
                  <span>S</span>
                </div>
                <div className="player">
                  <div className="username">{opponentUsername}</div>
                  <div className="tag">#{opponentTag}</div>
                </div>
                <div className="result">
                  <span>{win ? '+' : '-'}{Math.floor(win ? reward * 2 * 0.94 : reward)}</span>
                  <img src={zCoin} alt="z coin"/>
                </div>
              </div>
            ))}
          </div>}
        </> : <>
          <Reaptcha
            ref={this.recaptchaLink}
            size='invisible'
            theme='dark'
            sitekey='6LeCyuoUAAAAAMGcmT-2fbVO2g0fQU11W5fBlkUW'
            onVerify={this.changeLink.bind(this)}
          />
          <Reaptcha
            ref={this.recaptchaPassword}
            size='invisible'
            theme='dark'
            sitekey='6LeCyuoUAAAAAMGcmT-2fbVO2g0fQU11W5fBlkUW'
            onVerify={this.changePassword.bind(this)}
          />
          <Reaptcha
            ref={this.recaptchaRefCode}
            size='invisible'
            theme='dark'
            sitekey='6LeCyuoUAAAAAMGcmT-2fbVO2g0fQU11W5fBlkUW'
            onVerify={this.applyCode.bind(this)}
          />
          <Reaptcha
            ref={this.recaptchaClaimIncome}
            size='invisible'
            theme='dark'
            sitekey='6LeCyuoUAAAAAMGcmT-2fbVO2g0fQU11W5fBlkUW'
            onVerify={this.claimIncome.bind(this)}
          />
          <h3>
            {username}
            <img key={13} onClick={() => {
              this.setState({ showHistory: true })
            }} src={listSVG} alt="show activity"/>
            <img onClick={this.logout.bind(this)} className="logout" src={exitSVG} alt="logout"/>
          </h3>
          <div className="info">
            <div>{infoLeft}: {total_wins}</div>
            <div>
              {infoRight}: {total_winnings}
              <img src={zCoin} alt="z coin"/>
            </div>
          </div>
          <div className="blocks">
            <div className="block">
              <div className="heading">
                {linkTitle}
              </div>
              <InputField
                onKeyUp={e => {
                  if ( e.key === 'Enter' ) this.recaptchaLink.current.execute()
                }}
                onChange={e => {
                  this.setState({ inviteLink: e.target.value })
                }}
                value={inviteLink}
                type="text"
                placeholder={linkPlaceholder}
                autoComplete="off"
              />
              <button onClick={() => {
                this.recaptchaLink.current.execute()
              }}
                      className={`round-btn${!inviteLink ? ' disabled' : ''}`}>
                <span>{linkButton}</span>
              </button>
            </div>
            {invitedBy ?
              <div className="block">
                <div className="heading">
                  {refCodeTitle}
                </div>
                <div className="applied-message">
                  {appliedMessage}
                </div>
                <div className="invited-by">
                  {invitedByText}: {invitedBy}
                </div>
              </div> :
              <div className="block">
                <div className="heading">
                  {refCodeTitle}
                </div>
                <InputField
                  onKeyUp={e => {
                    if ( e.key === 'Enter' ) this.recaptchaRefCode.current.execute()
                  }}
                  onChange={e => {
                    this.setState({ refCode: e.target.value })
                  }}
                  value={refCode}
                  type="text"
                  placeholder={codePlaceholder}
                  autoComplete="off"
                  className="ref-code"
                />
                <button onClick={() => {
                  this.recaptchaRefCode.current.execute()
                }}
                        className={`round-btn${!this.validateRef.bind(this)() ? ' disabled' : ''}`}>
                  <span>{applyButton}</span>
                </button>
              </div>}
            <div className="block">
              <div className="heading">
                {passwordTitle}
              </div>
              <input type="text" style={{ display: 'none' }}/>
              <InputField
                onKeyUp={e => {
                  if ( e.key === 'Enter' ) this.recaptchaPassword.current.execute()
                }}
                onChange={e => {
                  this.setState({ oldPassword: e.target.value })
                }}
                value={oldPassword} type="password" placeholder={oldPasswordPlaceholder}/>
              <InputField
                onKeyUp={e => {
                  if ( e.key === 'Enter' ) this.recaptchaPassword.current.execute()
                }}
                onChange={e => {
                  this.setState({ newPassword1: e.target.value })
                }} value={newPassword1}
                type="password" placeholder={newPasswordPlaceholder}/>
              <InputField
                onKeyUp={e => {
                  if ( e.key === 'Enter' ) this.recaptchaPassword.current.execute()
                }}
                onChange={e => {
                  this.setState({ newPassword2: e.target.value })
                }} value={newPassword2} type="password" placeholder={confirmPasswordPlaceholder}/>
              <button onClick={() => {
                this.recaptchaPassword.current.execute()
              }}
                      className={`round-btn${!this.validate.bind(this)() ? ' disabled' : ''}`}>
                <span>{passwordButton}</span>
              </button>
            </div>
            <div className="block">
              <div className="heading">
                {referralInfoTitle}
              </div>
              <div className="info-row">
                {linkLabel}:
                <InputField
                  readonly={true}
                  value={`https://clashduelz.com/?r=${tag}`}
                />
              </div>
              <div className="info-row">
                {totalReferralsLabel}: {refs}
              </div>
              <div className="info-row">
                {referralIncomeLabel}: {Math.round((refEarnings + Number.EPSILON) * 100) / 100}
                <img src={zCoin} alt="z coin"/>
              </div>
              <button onClick={() => {
                this.recaptchaClaimIncome.current.execute()
              }}
                      className={`round-btn${!this.validateIncome.bind(this)() ? ' disabled' : ''}`}>
                <span>{claimButton}</span>
              </button>
            </div>
          </div>
        </>}
      </div>
    )
  }
}