import React from 'react'
import './styles.scss'

export const ErrorPage = () => (
  <div className="error-page">
    <div className="container">
      <div className="glitch" data-text="Error 404">Error 404</div>
      <div className="joke">Wow! Such empty...</div>
    </div>
  </div>
)