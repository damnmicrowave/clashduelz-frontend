import React from 'react'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'
import './styles.scss'

import blueBadge from './media/blue-badge.png'
import purpleBadge from './media/purple-badge.png'
import warning from './media/warning.svg'

const processString = require('react-process-string')


export const ClanRules = ({ texts }) => (
  <div className="clan-rules container">
    <h3>{texts.title}</h3>
    <div className="content">{texts.rules.map(({ heading, rules }, index) => (
      <div key={index} className="group">
        <h5>{heading}</h5>
        <ul>{rules.map((rule, index) => (
          <li key={index}>{
            processString([
              {
                regex: /RecoveryTeam/,
                fn: (key) => <Link key={key} to="/recovery_team/">RecoveryTeam</Link>
              },
              {
                regex: /RecoveryCW/,
                fn: (key) => <span key={key}>RecoveryCW</span>
              },
              {
                regex: /{{ratingLink}}/,
                fn: (key) => <Link key={key} to="/rating/">{texts.rating}</Link>
              },
              {
                regex: /{{blueBadge}}/,
                fn: (key) => <img key={key} src={blueBadge} alt="blue badge"/>
              },
              {
                regex: /{{purpleBadge}}/,
                fn: (key) => <img key={key} src={purpleBadge} alt="purple badge"/>
              },
              {
                regex: /{{warningIcon}}/,
                fn: (key) => <img key={key} src={warning} alt="warning"/>
              }
            ])(rule)}
          </li>
        ))}
        </ul>
      </div>
    ))}
    </div>
    <a className="round-btn" href="https://link.clashroyale.com/invite/clan/en?tag=YVQR9VJU&token=z8jghrd8">
      <span>{texts.joinClan}</span>
    </a>
  </div>
)

ClanRules.propTypes = {
  texts: PropTypes.shape({
    title: PropTypes.string.isRequired,
    rating: PropTypes.string.isRequired,
    joinClan: PropTypes.string.isRequired,
    rules: PropTypes.array.isRequired
  })
}
