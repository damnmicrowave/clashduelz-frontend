import React from 'react'
import { apiRequest } from '../../utils'
import './styles.scss'
import zCoin from './media/zCoin.svg'

export class Duels extends React.Component {
  startDuel() {
    const {
      texts,
      showPopup,
      setLoading,
      history,
      updateProfile
    } = this.props
    const {
      notAuthorizedPopup,
      duelsDisabledPopup,
      noRoomJoinedPopup,
      linkExpiredPopup,
      insufficientBalancePopup
    } = texts
    setLoading(true)
    apiRequest(({ status, reason }) => {
      setLoading(false)
      const reasons = {
        duelsDisabled: duelsDisabledPopup,
        notAuthorized: notAuthorizedPopup,
        noRoomJoined: noRoomJoinedPopup,
        linkExpired: linkExpiredPopup,
        insufficientBalance: insufficientBalancePopup
      }
      if ( status === 'error' ) {
        showPopup(reasons[reason])
        if ( reason === 'linkExpired' ) history.push('/profile/')
      } else {
        updateProfile({ status: 'waiting' })
      }
    }, 'start_duel')
  }

  render() {
    const {
      texts,
      online,
      currentRoom,
      enterRoom
    } = this.props
    const {
      title,
      depositAmount,
      button,
      info,
      bottomInfo
    } = texts
    const rooms = [ 5, 25, 50, 100, 250, 500 ].map(i => i.toString())
    return (
      <div className="duels container">
        <h3>
          {title}
        </h3>
        <div className="info">
          {depositAmount}:
        </div>
        <div className="rooms">
          {rooms.map((item, index) => (
            <div onClick={() => {
              enterRoom(item)
            }} key={index}
                 className={`room${currentRoom === item ? ' active' : ''}`}>
                                <span className="amount">
                                    {item}
                                  <img src={zCoin} alt="z coin"/>
                                </span>
              <div className="online-wrapper">
                <div className="online">
                  online
                  <i/>
                  <div>{online[item]}</div>
                </div>
              </div>
            </div>
          ))}
        </div>
        <button onClick={this.startDuel.bind(this)} className={`round-btn${!currentRoom ? ' disabled' : ''}`}>
          <span>{button}</span>
        </button>
        <div className="profit">
          {info}: {currentRoom ? Math.floor(currentRoom * 2 * 0.94) : 0}
          <img src={zCoin} alt="z coin"/>
        </div>
        <div className="current-online">{bottomInfo}: {online.general}</div>
      </div>
    )
  }
}
