import React from 'react'
import { NavLink } from 'react-router-dom'
import { getParents } from '../../utils'
import './styles.scss'
import timesSVG from './media/times.svg'
import menuSVG from './media/menu.svg'
import clashduelzSVG from './media/clashduelz.svg'
import { ReactComponent as TelegramSVG } from './media/telegram.svg'


class LanguagePicker extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      expanded: false
    }

    this.list = React.createRef()
  }

  componentDidMount() {
    window.addEventListener('click', e => {
      if ( !getParents(e.target).includes(this.list.current) ) this.setState({ expanded: false })
    })
  }

  selectLang(lang) {
    this.props.selectLang(lang)
    this.setState({ expanded: false })
  }

  render() {
    const {
      lang
    } = this.props
    return (
      <div ref={this.list} className={`language${this.state.expanded ? ' expanded' : ''}`}>
        <span onClick={() => {
          this.setState({ expanded: true })
        }}>{lang}</span>
        <div className="variants">
          <div className={lang === 'EN' ? 'active' : ''} onClick={() => {
            this.selectLang.bind(this)('EN')
          }}>EN
          </div>
          <div className={lang === 'RU' ? 'active' : ''} onClick={() => {
            this.selectLang.bind(this)('RU')
          }}>RU
          </div>
        </div>
      </div>
    )
  }
}

export class ClanNavbar extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      sliderToggled: false
    }

    this.slider = React.createRef()
    this.sliderToggler = React.createRef()
  }

  componentDidMount() {
    window.addEventListener('click', e => {
      const parents = getParents(e.target)
      if (
        !parents.includes(this.slider.current) &&
        !parents.includes(this.sliderToggler.current) &&
        this.state.sliderToggled
      ) {
        this.toggleSlider()
      }
    })
  }

  toggleSlider() {
    this.setState({ sliderToggled: !this.state.sliderToggled })
  }

  render() {
    const { sliderToggled } = this.state
    const {
      rules,
      rating,
      recoveryTeam,
      mainSite
    } = this.props.texts
    const NavLinkMobile = props => (
      <NavLink onClick={this.toggleSlider.bind(this)} {...props}/>
    )
    return (
      <div className='navbar-clan'>
        <div className="wrapper">
          <NavLink to="/" className="logo-wrapper">
            <div className="logo">
              <img src={clashduelzSVG} alt="logo"/>
              <div className="clan">
                <span>Clan</span>
              </div>
            </div>
          </NavLink>
          <div className="links">
            <NavLink exact to="/">{rules}</NavLink>
            <NavLink exact to="/rating/">{rating}</NavLink>
            {/*<NavLink exact to="/cw_info/">{clanWarsCards}</NavLink>*/}
            <NavLink exact to="/recovery_team/">{recoveryTeam}</NavLink>
          </div>
          <div className="side-panel">
            <a href="https://clashduelz.com/" className="mainpage">{mainSite}</a>
            <a href="https://t.me/joinchat/EPsL9Rf8GCG19IW_E_u5TQ" target="_blank" rel="noopener noreferrer"
               className="mainpage">
              <TelegramSVG/>
            </a>
            <LanguagePicker {...this.props}/>
          </div>
          <span ref={this.sliderToggler} onClick={this.toggleSlider.bind(this)} className="menu-toggler">
                        <img src={menuSVG} alt="open menu"/>
                    </span>
        </div>
        <div ref={this.slider} className={`slider${sliderToggled ? ' active' : ''}`}>
                    <span onClick={this.toggleSlider.bind(this)} className="close-menu">
                        <img src={timesSVG} alt="close menu"/>
                    </span>
          <div className="links">
            <NavLinkMobile exact to="/">{rules}</NavLinkMobile>
            <NavLinkMobile exact to="/rating/">{rating}</NavLinkMobile>
            {/*<NavLinkMobile exact to="/cw_info/">{clanWarsCards}</NavLinkMobile>*/}
            <NavLinkMobile exact to="/recovery_team/">{recoveryTeam}</NavLinkMobile>
            <div className="side-panel">
              <a href="https://clashduelz.com/" className="mainpage">{mainSite}</a>
              <a href="https://t.me/joinchat/EPsL9Rf8GCG19IW_E_u5TQ" className="mainpage">Telegram</a>
              <LanguagePicker {...this.props}/>
            </div>
          </div>
        </div>
      </div>
    )
  }
}