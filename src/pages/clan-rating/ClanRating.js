import React from 'react'
import { apiRequest } from '../../utils'
import { Loading } from '../../includes'

import './styles.scss'

import warningSVG from './media/warning.svg'


export class ClanRating extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      loaded: false,
      members: []
    }
  }

  componentDidMount() {
    apiRequest(({ members }) => {
      this.setState({
        loaded: true,
        members
      })
    }, 'get_ratinglist')
  }

  updateRating() {
    apiRequest(({ status, members }) => {
      if ( status === 'success' ) {
        this.setState({ members })
        alert('Success !')
      } else {
        alert('Fail ):')
      }
    }, 'update_rating')
  }

  updateRoles() {
    apiRequest(({ status, members }) => {
      if ( status === 'success' ) {
        this.setState({ members })
        alert('Success !')
      } else {
        alert('Fail ):')
      }
    }, 'update_roles')
  }

  seasonReset() {
    apiRequest(
      ({ status, members }) => {
        if ( status === 'success' ) {
          this.setState({ members })
          alert('Success !')
        } else {
          alert('Fail ):')
        }
      }, 'reset_rating')
  }

  render() {
    const {
      texts,
      loggedIn
    } = this.props
    const {
      title,
      usernameColumn,
      scoreColumn
    } = texts
    const {
      loaded,
      members
    } = this.state
    return (
      <>
        {!loaded && <Loading/>}
        <div className="rating container">
          <h3>
            {title}
          </h3>
          {loaded &&
          <div className="board">
            <div className="row">
              <div className="number">№</div>
              <div className="username">{usernameColumn}</div>
              <div className="score">{scoreColumn}</div>
            </div>
            <div className="divider"/>
            {members.sort((a, b) => b.rating - a.rating)
              .map((member, index) => {
                const {
                  id,
                  nickname,
                  name,
                  role,
                  warnings,
                  rating
                } = member
                return <React.Fragment key={id}>
                  <div className="row">
                    <div className="number">{index + 1}</div>
                    <div className="username">
                      <div>{nickname} {name && <>({name})</>}</div>
                      <div className="info">
                        <span>{texts[role]}</span>
                        {Boolean(warnings) && <>{[ ...Array(warnings).keys() ].map(
                          (key) => <img key={key} src={warningSVG} alt="warning"/>
                        )}</>}
                      </div>
                    </div>
                    <div className="score">{rating}</div>
                  </div>
                  <div className="divider"/>
                </React.Fragment>
              })}
          </div>}
          {loggedIn && <>
            <button onClick={this.updateRating.bind(this)} className="round-btn">
              <span>Update rating</span>
            </button>
            <button onClick={this.updateRoles.bind(this)} className="round-btn">
              <span>Update roles</span>
            </button>
            <button onClick={this.seasonReset.bind(this)} className="round-btn blue">
              <span>Season reset</span>
            </button>
          </>}
        </div>
      </>
    )
  }
}