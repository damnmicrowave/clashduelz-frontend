import React from 'react'
import ym from 'react-yandex-metrika'
import { Loading, Popup } from '../../includes'
import { Link } from 'react-router-dom'
import { apiRequest } from '../../utils'
import './styles.scss'
import zCoin from './media/zCoin.svg'

export class Duel extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      loaded: false,
      popupMessage: '',
      finished: false,
      draw: false,
      winner: '',
      stake: 5,
      inviteLink: '',
      players: [],
      newProfileInfo: {}
    }
  }

  componentDidMount() {
    const {
      texts,
      profile,
      showPopup,
      roomSocket,
      loading
    } = this.props
    const {
      notAuthorizedPopup,
      notInDuelPopup,
      doesNotExistPopup,
      notPlayedPopup
    } = texts
    if ( !loading && roomSocket ) {
      apiRequest(({ status, reason, result, stake, opponents }) => {
        if ( status === 'error' ) {
          const reasons = {
            notAuthorized: notAuthorizedPopup,
            notInDuel: notInDuelPopup,
            doesNotExist: doesNotExistPopup
          }
          showPopup(reasons[reason], true)
        } else {
          ym('reachGoal', 'joinedDuel')
          roomSocket.onmessage = e => {
            const { message, response } = JSON.parse(e.data).data
            if ( message === 'DUEL_FINISHED' ) {
              const {
                winner,
                profile,
                result
              } = response
              if ( result === 'draw' ) {
                this.setState({
                  loaded: true,
                  finished: true,
                  draw: true,
                  newProfileInfo: profile
                })
              } else {
                this.setState({
                  loaded: true,
                  finished: true,
                  winner: winner,
                  newProfileInfo: profile
                })
              }
            }
          }
          if ( result === 'notPlayed' ) {
            showPopup(notPlayedPopup)
          } else {
            this.setState({
              loaded: true,
              stake: stake,
              inviteLink: opponents.find(o => o.tag !== profile.tag).link,
              players: opponents
            })
          }
        }
      }, 'get_duel_info')
    }
  }

  checkResults() {
    const {
      texts,
      showPopup
    } = this.props
    const {
      notAuthorizedPopup,
      notInDuelPopup,
      doesNotExistPopup,
      notPlayedPopup
    } = texts
    this.setState({ loaded: false })
    apiRequest(({ status, reason, result, winner, profile }) => {
      if ( status === 'error' ) {
        this.setState({ loaded: true })
        const reasons = {
          notAuthorized: notAuthorizedPopup,
          notInDuel: notInDuelPopup,
          doesNotExist: doesNotExistPopup
        }
        showPopup(reasons[reason], true)
      } else {
        if ( result === 'notPlayed' ) {
          showPopup(notPlayedPopup)
          this.setState({
            loaded: true,
            popupMessage: notPlayedPopup
          }, () => {
            setTimeout(() => {
              this.setState({ popupMessage: '' })
            }, 5000)
          })
        } else {
          if ( result === 'draw' ) {
            this.setState({
              loaded: true,
              finished: true,
              draw: true,
              newProfileInfo: profile
            })
          } else {
            this.setState({
              loaded: true,
              finished: true,
              winner: winner,
              newProfileInfo: profile
            })
          }
        }
      }
    }, 'check_duel_results')
  }

  render() {
    const {
      texts,
      updateProfile
    } = this.props
    const {
      duelTitle,
      duelTitleFinished,
      atStake,
      reward,
      openGameButton,
      duelInstructions,
      checkResultsButton,
      winnerText,
      drawText,
      bottomLink,
      issuesLink
    } = texts
    const {
      loaded,
      popupMessage,
      stake,
      inviteLink,
      players,
      finished,
      winner,
      draw,
      newProfileInfo
    } = this.state
    return !loaded ? <Loading/> :
      <>
        {popupMessage && <Popup message={popupMessage}/>}
        <div className="duel container">
          <h4>{!finished ? duelTitle : duelTitleFinished}</h4>
          <div className="stake">
            {!finished ? atStake : reward}:<span>{Math.floor(stake * 2 * 0.94)}<img src={zCoin}
                                                                                    alt="z coin"/></span>
          </div>
          <div className="opponents">
            <div className="username">
              <span>{players[0].username}</span>
              <span>#{players[0].tag}</span>
            </div>
            <div className="vs">
              <span>V</span>
              <span>S</span>
            </div>
            <div className="username">
              <span>{players[1].username}</span>
              <span>#{players[1].tag}</span>
            </div>
          </div>
          {!finished ?
            <>
              <a href={inviteLink} target="_blank" rel="noreferrer noopener" className="round-btn">
                <span>{openGameButton}</span>
              </a>
              <div className="info">
                {duelInstructions}
              </div>
              <button onClick={this.checkResults.bind(this)} className="round-btn purple">
                <span>{checkResultsButton}</span>
              </button>
              <Link className="contact-support" to="/support/">{issuesLink}</Link>
            </> :
            <>
              <div className="winner">
                {!draw ? <>
                  {winner}<br/><span>{winnerText}</span>
                </> : <span>{drawText}</span>}
              </div>
              <button onClick={() => {
                updateProfile(newProfileInfo)
              }} className="mainpage-link">{bottomLink}</button>
            </>}
        </div>
      </>
  }
}
