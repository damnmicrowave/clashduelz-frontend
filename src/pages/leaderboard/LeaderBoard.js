import React from 'react'
import { apiRequest } from '../../utils'
import { Loading } from '../../includes'
import './styles.scss'
import zCoin from './media/zCoin.svg'

export class LeaderBoard extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      loaded: false,
      topPlayers: [],
      yourPlace: undefined,
      yourUsername: undefined,
      yourWinnings: undefined
    }
  }

  componentDidMount() {
    apiRequest(({ topPlayers, yourPlace, yourWinnings }) => {
      this.setState({
        loaded: true,
        topPlayers: topPlayers,
        yourPlace: yourPlace,
        yourWinnings: yourWinnings
      })
    }, 'get_leaderboard')
  }

  render() {
    const {
      title,
      leftColumn,
      rightColumn,
      bottomRow
    } = this.props.texts
    const {
      loaded,
      topPlayers,
      yourPlace,
      yourWinnings
    } = this.state
    return (
      <>
        {!loaded && <Loading/>}
        <div className="leaderboard container">
          <h3>
            {title}
          </h3>
          {loaded &&
          <div className="board">
            <div className="row">
              <div className="number">№</div>
              <div className="username">{leftColumn}</div>
              <div className="amount">{rightColumn}</div>
            </div>
            <div className={`divider${yourPlace === 0 ? ' purple' : ''}`}/>
            {topPlayers.map(({ username, total_winnings }, index) => (
              <React.Fragment key={index}>
                <div className="row">
                  <div className="number">{index + 1}</div>
                  <div className="username">{index === yourPlace ? bottomRow : username}</div>
                  <div className="amount">
                    {total_winnings}
                    <img src={zCoin} alt="z coin"/>
                  </div>
                </div>
                <div className={`divider${index + 1 === yourPlace || index === yourPlace ? ' purple' : ''}`}/>
              </React.Fragment>
            ))}
            {yourPlace > 9 && <>
              {yourPlace > 10 &&
              <div className="row empty">
                <div>. . .</div>
              </div>}
              <div className="row">
                <div className="number">{yourPlace + 1}</div>
                <div className="username">{bottomRow}</div>
                <div className="amount">
                  {yourWinnings}
                  <img src={zCoin} alt="z coin"/>
                </div>
              </div>
            </>}
          </div>}
        </div>
      </>
    )
  }
}