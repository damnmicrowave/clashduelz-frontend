import React from 'react'
import { Redirect, Route, Switch } from 'react-router'
import { ErrorPage } from '..'
import {
  BalancePage,
  PaymentsPage,
  WithdrawsPage
} from './components'
import './styles.scss'


export class Balance extends React.Component {
  render() {
    return (
      <>
        {!this.props.loggedIn && <Redirect to='/login/'/>}
        <div className="deposit container">
          <Switch>
            <Route exact path="/balance/payments/" render={
              props => <PaymentsPage {...props} {...this.props}/>
            }/>
            <Route exact path="/balance/withdraws/" render={
              props => <WithdrawsPage {...props} {...this.props}/>
            }/>
            <Route exact path="/balance/" render={
              props => <BalancePage {...props} {...this.props}/>
            }/>
            <Route path="/" render={() => <ErrorPage/>}/>
          </Switch>
        </div>
      </>
    )
  }
}