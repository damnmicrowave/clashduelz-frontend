import React from 'react'
import ym from 'react-yandex-metrika'
import Reaptcha from 'reaptcha'
import { Link } from 'react-router-dom'
import { apiRequest } from '../../../utils'
import { InputField } from '../../../includes'
import zCoin from '../media/zCoin.svg'
import listSVG from '../media/list.svg'
import qiwiSVG from '../media/qiwi.svg'
import yandexSVG from '../media/yandex.svg'
import webmoneySVG from '../media/webmoney.svg'
import visaMastercardSVG from '../media/visa-mastercard.svg'
import bitcoinSVG from '../media/bitcoin.svg'


export class BalancePage extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      depositAmount: '',
      withdrawAmount: '',
      selectedOption: '',
      accountNumber: '',
      withdrawPercentage: 0
    }

    this.recaptchaBuy = React.createRef()
    this.recaptchaSell = React.createRef()
    this.scroller = React.createRef()
  }

  deposit(token) {
    const {
      texts,
      showPopup,
      setLoading
    } = this.props
    const {
      notAuthenticatedPopup,
      lowAmountPopup,
      largeAmountPopup,
      incorrectFormatPopup,
      captchaFailedPopup
    } = texts
    setLoading(true)
    apiRequest(({ status, captchaError, reason, paymentUrl }) => {
      if ( captchaError ) {
        showPopup(captchaFailedPopup)
      } else if ( status === 'ok' ) {
        ym('reachGoal', 'balanceReplenishment')
        window.location.replace(paymentUrl)
      } else {
        setLoading(false)
        const reasons = {
          notAuthenticated: notAuthenticatedPopup,
          lowAmount: lowAmountPopup,
          largeAmount: largeAmountPopup,
          incorrectFormat: incorrectFormatPopup
        }
        showPopup(reasons[reason])
      }
    }, 'get_payment_link', {
      method: 'POST',
      data: {
        amount: this.state.depositAmount,
        token: token
      }
    })
  }

  validateDeposit() {
    const { depositAmount } = this.state
    return /\d+|\d+.\d+/.test(depositAmount) && parseFloat(depositAmount) >= 0.25
  }

  withdraw(token) {
    const {
      texts,
      showPopup,
      setLoading,
      updateProfile
    } = this.props
    const {
      notAuthorizedPopup,
      invalidFormatPopup,
      notEnoughZCoinsPopup,
      amountTooLargePopup,
      amountTooLowPopup,
      unsupportedMethodPopup,
      withdrawSuccessful,
      captchaFailedPopup
    } = texts
    const {
      withdrawAmount,
      selectedOption,
      accountNumber
    } = this.state
    setLoading(true)
    apiRequest(({ status, captchaError, reason, balance }) => {
      setLoading(false)
      if ( captchaError ) {
        showPopup(captchaFailedPopup)
      } else if ( status === 'ok' ) {
        this.setState({
          withdrawAmount: '',
          selectedOption: ''
        })
        showPopup(withdrawSuccessful)
        updateProfile({ balance: balance })
      } else {
        const reasons = {
          notAuthenticated: notAuthorizedPopup,
          invalidFormat: invalidFormatPopup,
          notEnoughZCoins: notEnoughZCoinsPopup,
          amountTooLarge: amountTooLargePopup,
          amountTooLow: amountTooLowPopup,
          unsupportedMethod: unsupportedMethodPopup
        }
        showPopup(reasons[reason])
      }
    }, 'request_withdraw', {
      method: 'POST',
      data: {
        amount: withdrawAmount,
        method: selectedOption,
        wallet: accountNumber,
        token: token
      }
    })
  }

  validateWithdraw() {
    const {
      withdrawAmount,
      selectedOption,
      accountNumber
    } = this.state
    const regex = {
      qiwi: /^\d{8,15}$/,
      yandex: /^4100\d{12,16}$/,
      webmoney: /^[ZRHXGLVKYEB]\d{12}$/,
      card: /^\d{16,}$/,
      bitcoin: /^\b((bc|tb)(0([ac-hj-np-z02-9]{39}|[ac-hj-np-z02-9]{59})|1[ac-hj-np-z02-9]{8,87})|([13]|[mn2])[a-km-zA-HJ-NP-Z1-9]{25,39})\b$/
    }
    return /\d+/.test(withdrawAmount) && parseInt(withdrawAmount) >= 100 && selectedOption && regex[selectedOption].test(accountNumber)
  }

  render() {
    const {
      depositTitle,
      depositPlaceholder,
      depositButton,
      depositInfo,
      withdrawTitle,
      withdrawPlaceholder,
      withdrawComission,
      withdrawButton,
      withdrawInfo
    } = this.props.texts
    const {
      depositAmount,
      withdrawAmount,
      withdrawPercentage,
      selectedOption,
      accountNumber
    } = this.state
    const depositValid = this.validateDeposit()
    const withdrawValid = this.validateWithdraw()
    const options = [
      {
        id: 'qiwi',
        comission: '2%',
        percentage: 0.02,
        logo: qiwiSVG
      },
      {
        id: 'yandex',
        comission: '2%',
        percentage: 0.02,
        logo: yandexSVG
      },
      {
        id: 'webmoney',
        comission: '4%',
        percentage: 0.04,
        logo: webmoneySVG
      },
      {
        id: 'card',
        comission: '2%<br/>(min 3$)',
        percentage: 0.02,
        logo: visaMastercardSVG
      },
      {
        id: 'bitcoin',
        comission: '13%',
        percentage: 0.13,
        logo: bitcoinSVG
      }
    ]
    const alphabet = 'abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ123456789'
    const walletPlaceholder = {
      qiwi: `7${(Math.random() * 10000000000).toFixed()}`,
      yandex: `4100${(Math.random() * 100000000000).toFixed()}`,
      webmoney: `Z${(Math.random() * 1000000000000).toFixed()}`,
      card: `${(Math.random() * 10000000000000000).toFixed()}`,
      bitcoin: `${Math.random() < 0.3 ? 1 : 3}${[ ...Array(Math.floor(Math.random() * 7 + 27)).keys() ].map(() => alphabet.charAt(Math.floor(Math.random() * alphabet.length))).join('')}`
    }
    let minWithdraw = 100
    if ( selectedOption === 'card' ) minWithdraw = 600
    let withdrawDollars = withdrawAmount * withdrawPercentage * .01
    if ( selectedOption === 'card' ) withdrawDollars = Math.min(withdrawAmount * withdrawPercentage * .01, withdrawAmount * .01 - 3)
    return (
      <>
        <Reaptcha
          ref={this.recaptchaBuy}
          size='invisible'
          theme='dark'
          sitekey='6LeCyuoUAAAAAMGcmT-2fbVO2g0fQU11W5fBlkUW'
          onVerify={this.deposit.bind(this)}
        />
        <Reaptcha
          ref={this.recaptchaSell}
          size='invisible'
          theme='dark'
          sitekey='6LeCyuoUAAAAAMGcmT-2fbVO2g0fQU11W5fBlkUW'
          onVerify={this.withdraw.bind(this)}
        />
        <h3>
          {depositTitle}
          <Link className="right" to="/balance/payments/"><img src={listSVG} alt="payments"/></Link>
        </h3>
        <InputField
          type="number"
          step={0.01}
          min={0.25}
          max={200}
          value={depositAmount}
          placeholder={depositPlaceholder}
          onChange={e => {
            this.setState({ depositAmount: e.target.value })
          }}
        />
        <div className="desc">
          1$ = 100<img src={zCoin} alt="z coin"/>
        </div>
        <button onClick={() => {
          this.recaptchaBuy.current.execute()
        }}
                className={depositValid ? 'round-btn' : 'round-btn disabled'}>
                        <span>
                            {depositButton} {depositValid && <>
                          {(depositAmount * 100).toFixed()}
                          <img src={zCoin} alt="z coin"/>
                        </>}
                        </span>
        </button>
        <div className="desc">
          {depositInfo}
        </div>
        <h3 style={{ marginTop: '4rem' }}>
          {withdrawTitle}
          <Link className="right" to="/balance/withdraws/"><img src={listSVG} alt="withdraws"/></Link>
        </h3>
        <InputField
          type="number"
          step={1}
          min={minWithdraw}
          max={this.props.balance}
          value={withdrawAmount}
          placeholder={withdrawPlaceholder}
          onChange={e => {
            this.setState({ withdrawAmount: e.target.value })
          }}
        />
        <div className="scroller-wrapper">
          <div ref={this.scroller} onWheel={(e) => {
            this.scroller.current.scrollLeft += Number(e.deltaY) * 0.5
          }} className="withdraw-options">
            {options.map(({ id, comission, percentage, logo }, index) => (
              <div key={index} onClick={() => {
                this.setState({
                  selectedOption: id,
                  withdrawPercentage: 1 - percentage,
                  accountNumber: ''
                })
              }} className={`option${id === selectedOption ? ' active' : ''}`}>
                                <span>
                                    <div className="logo"
                                         style={{ maskImage: `url(${logo})`, WebkitMaskImage: `url(${logo})` }}/>
                                    <div dangerouslySetInnerHTML={{ __html: `${withdrawComission}: ${comission}` }}/>
                                </span>
              </div>
            ))}
          </div>
        </div>
        {selectedOption && <>
          <div className="subtitle">Wallet/Card number:</div>
          <InputField
            type="text"
            value={accountNumber}
            placeholder={`E.g. ${walletPlaceholder[selectedOption]}`}
            onChange={e => {
              this.setState({ accountNumber: e.target.value })
            }}
          />
        </>}
        <button onClick={() => {
          this.recaptchaSell.current.execute()
        }}
                className={withdrawValid ? 'round-btn' : 'round-btn disabled'}>
                        <span>
                            {withdrawButton} {withdrawValid && <>
                          {withdrawDollars.toFixed(2)}$
                        </>}
                        </span>
        </button>
        <div className="desc">
          {withdrawInfo} {minWithdraw}
          <img src={zCoin} alt="z coin"/>
        </div>
      </>
    )
  }
}