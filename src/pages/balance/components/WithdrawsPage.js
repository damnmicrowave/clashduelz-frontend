import React from 'react'
import { apiRequest } from '../../../utils'
import { Loading } from '../../../includes'
import { Link } from 'react-router-dom'
import arrowSVG from '../media/arrow.svg'


export class WithdrawsPage extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      loading: true,
      withdraws: []
    }
  }

  componentDidMount() {
    if ( this.state.loading ) {
      apiRequest(({ status, withdraws }) => {
        if ( status === 'ok' ) {
          this.setState({ withdraws: withdraws, loading: false })
        } else {
          this.setState({ loading: false })
          this.props.history.push('/login/')
        }
      }, 'get_withdraws')
    }
  }

  render() {
    const {
      withdraws,
      loading
    } = this.state
    const {
      withdrawsTitle,
      paymentAmount,
      paymentStatus,
      withdrawMethod,
      withdrawWallet,
      statusInQueue,
      statusProcessing,
      statusSuccess,
      statusFailed
    } = this.props.texts
    const statuses = {
      'in queue': statusInQueue,
      processing: statusProcessing,
      success: statusSuccess,
      failed: statusFailed
    }
    return (
      <>
        {loading && <Loading/>}
        <h3>
          <Link to="/balance/"><img src={arrowSVG} alt="back"/></Link>
          {withdrawsTitle}
        </h3>
        {!loading && <div className="withdraws-list">
          {withdraws.map(({ amount_usd, status, method, wallet }, index) => (
            <div key={index} className="withdraw">
              <div className="row">
                <div className="amount">
                  <span>{paymentAmount}:</span>
                  {(amount_usd).toFixed(2)}$
                </div>
                <div className={`status ${status}`}>
                  {paymentStatus}: <span>{statuses[status]}</span>
                </div>
              </div>
              <div className="row" style={{ fontSize: 16, height: '2rem' }}>
                <span style={{ marginRight: '1rem' }}>{withdrawMethod}: {method}</span>
                <span style={{ marginLeft: 'auto' }}>{withdrawWallet}: {wallet}</span>
              </div>
            </div>
          ))}
        </div>}
      </>
    )
  }
}