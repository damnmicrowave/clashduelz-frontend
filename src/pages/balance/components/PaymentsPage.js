import React from 'react'
import { apiRequest } from '../../../utils'
import { Loading } from '../../../includes'
import { Link } from 'react-router-dom'
import zCoin from '../media/zCoin.svg'
import arrowSVG from '../media/arrow.svg'


export class PaymentsPage extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      loading: true,
      payments: []
    }
  }

  componentDidMount() {
    if ( this.state.loading ) {
      apiRequest(({ status, payments }) => {
        if ( status === 'ok' ) {
          this.setState({ payments: payments, loading: false })
        } else {
          this.setState({ loading: false })
          this.props.history.push('/login/')
        }
      }, 'get_payments')
    }
  }

  render() {
    const {
      payments,
      loading
    } = this.state
    const {
      paymentsTitle,
      paymentAmount,
      paymentStatus,
      statusOngoing,
      statusSuccess,
      statusFailed
    } = this.props.texts
    const statuses = {
      ongoing: statusOngoing,
      success: statusSuccess,
      failed: statusFailed
    }
    return (
      <>
        {loading && <Loading/>}
        <h3>
          <Link to="/balance/"><img src={arrowSVG} alt="back"/></Link>
          {paymentsTitle}
        </h3>
        {!loading && <div className="payments-list">
          {payments.map(({ amount, status }, index) => (
            <div key={index} className="payment">
              <div className="amount">
                <span>{paymentAmount}:</span>
                {(amount * 100).toFixed()}
                <img src={zCoin} alt="z coin"/>
              </div>
              <div className={`status ${status}`}>
                {paymentStatus}: <span>{statuses[status]}</span>
              </div>
            </div>
          ))}
        </div>}
      </>
    )
  }
}