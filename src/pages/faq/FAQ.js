import React from 'react'
import './styles.scss'
import arrowSVG from './media/arrow.svg'
import { Link } from 'react-router-dom'

const processString = require('react-process-string')

class Question extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      toggled: false
    }
  }

  render() {
    const { toggled } = this.state
    const {
      question,
      answer,
      profileLabel,
      supportLabel
    } = this.props
    return (
      <div onClick={() => {
        this.setState({ toggled: !toggled })
      }} className={`question${toggled ? ' active' : ''}`}>
        <div className="q">{question}</div>
        <img src={arrowSVG} alt="arrow"/>
        <div className="answer">
          {
            processString([
              {
                regex: /{{profileLink}}/,
                fn: (key) => <Link key={key} to="/profile/">{profileLabel}</Link>
              },
              {
                regex: /{{supportLink}}/,
                fn: (key) => <Link key={key} to="/support/">{supportLabel}</Link>
              }
            ])(answer)}
        </div>
      </div>
    )
  }
}

export class FAQ extends React.Component {
  render() {
    const { texts } = this.props
    const {
      title,
      questions,
      profileLabel,
      supportLabel
    } = texts
    return (
      <div className="faq container">
        <h3>{title}</h3>
        <div className="questions">
          {questions.map(({ question, answer }, index) => (
            <Question
              key={index}
              question={question}
              answer={answer}
              profileLabel={profileLabel}
              supportLabel={supportLabel}
            />
          ))}
        </div>
      </div>
    )
  }
}