import React from 'react'
import { Redirect, Route, Switch } from 'react-router'
import './styles.scss'
import {
  EnterTag,
  ConfirmAccount,
  SetPassword,
  ConfirmPassword,
  SetLink
} from './steps'

export class Login extends React.Component {
  render() {
    const { step } = this.props.match.params
    if ( this.props.loggedIn ) return <Redirect to="/"/>
    if ( !step ) return <Redirect to='/login/enter_tag/'/>
    return (
      <div className="login container">
        <Switch>
          <Route exact path="/login/enter_tag/" render={
            props => <EnterTag {...props} {...this.props}/>
          }/>
          <Route exact path="/login/confirm_account/" render={
            props => <ConfirmAccount {...props} {...this.props}/>
          }/>
          <Route exact path="/login/set_password/" render={
            props => <SetPassword {...props} {...this.props}/>
          }/>
          <Route exact path="/login/invite_link/" render={
            props => <SetLink {...props} {...this.props}/>
          }/>
          <Route exact path="/login/confirm_password/" render={
            props => <ConfirmPassword {...props} {...this.props}/>
          }/>
        </Switch>
      </div>
    )
  }
}
