import React from 'react'
import { apiRequest } from '../../../utils'
import { Redirect } from 'react-router'
import { InputField } from '../../../includes'
import Reaptcha from 'reaptcha'

export class SetPassword extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      password1: '',
      password2: ''
    }

    this.recaptchaRef = React.createRef()
  }

  setPassword(token) {
    const { tag } = this.props.location.state.profile
    const {
      setLoading,
      showPopup,
      history,
      texts
    } = this.props
    const {
      setPasswordDifferPopup,
      setPasswordShortPopup,
      captchaFailedPopup,
      errorPopup,
      resetPasswordDone
    } = texts
    setLoading(true)
    apiRequest(({ status, reason, captchaError, redirectToStage, profile, skipLink }) => {
      setLoading(false)
      if ( captchaError ) {
        showPopup(captchaFailedPopup)
      } else if ( status === 'ok' ) {
        if ( skipLink ) showPopup(resetPasswordDone, true)
        history.push({
          pathname: skipLink ? '/' : `/login/${redirectToStage}/`,
          state: { profile: profile }
        })
      } else if ( reason ) {
        const reasons = {
          passwordsMismatch: setPasswordDifferPopup,
          passwordTooShort: setPasswordShortPopup
        }
        showPopup(reasons[reason])
      } else {
        showPopup(errorPopup)
      }
    }, 'set_password', {
      method: 'POST',
      data: {
        ...this.state,
        tag: tag,
        token: token
      }
    })
  }

  render() {
    const {
      password1,
      password2
    } = this.state
    if ( !this.props.location.state ) return <Redirect to='/login/enter_tag/'/>
    const {
      setPasswordTitle,
      setPasswordPlaceholderNew,
      setPasswordPlaceholderConfirm,
      setPasswordButton
    } = this.props.texts
    return (
      <>
        <Reaptcha
          ref={this.recaptchaRef}
          size='invisible'
          theme='dark'
          sitekey='6LeCyuoUAAAAAMGcmT-2fbVO2g0fQU11W5fBlkUW'
          onVerify={this.setPassword.bind(this)}
        />
        <h3>
          {setPasswordTitle}
        </h3>
        <InputField onKeyUp={e => {
          if ( e.key === 'Enter' ) this.recaptchaRef.current.execute()
        }}
                    onChange={e => {
                      this.setState({ password1: e.target.value })
                    }}
                    value={password1}
                    type="password" placeholder={setPasswordPlaceholderNew}/>
        <InputField onKeyUp={e => {
          if ( e.key === 'Enter' ) this.recaptchaRef.current.execute()
        }}
                    onChange={e => {
                      this.setState({ password2: e.target.value })
                    }}
                    value={password2}
                    type="password" placeholder={setPasswordPlaceholderConfirm}/>
        <button onClick={() => {
          this.recaptchaRef.current.execute()
        }}
                className={`round-btn${password1 === password2 && password2 ? '' : ' disabled'}`}>
          <span>{setPasswordButton}</span>
        </button>
      </>
    )
  }
}