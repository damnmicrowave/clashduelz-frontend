import React from 'react'
import { apiRequest } from '../../../utils'
import { InputField } from '../../../includes'
import Reaptcha from 'reaptcha'

export class EnterTag extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      tag: ''
    }

    this.recaptchaRef = React.createRef()
  }

  findAccount(token) {
    const {
      setLoading,
      showPopup,
      history,
      texts
    } = this.props
    const {
      notFoundPopup,
      captchaFailedPopup,
      errorPopup
    } = texts
    setLoading(true)
    apiRequest(({ status, reason, captchaError, redirectToStage, profile }) => {
      setLoading(false)
      if ( status === 'ok' ) {
        history.push({
          pathname: `/login/${redirectToStage}/`,
          state: { profile: profile }
        })
      } else if ( reason === 'notFound' ) {
        showPopup(notFoundPopup)
      } else if ( captchaError ) {
        showPopup(captchaFailedPopup)
      } else {
        showPopup(errorPopup)
      }
    }, 'find_account', {
      method: 'POST',
      data: {
        tag: this.state.tag.replace('#', '').trim(),
        referralCode: window.localStorage.getItem('referralCode'),
        token: token
      }
    })
  }

  validate() {
    const regex = /^#([qcjgvlpuryQCJGVLPURY2908]+){4,}$/
    return regex.test(this.state.tag)
  }

  render() {
    const {
      enterTagTitle,
      enterTagButton
    } = this.props.texts
    return (
      <>
        <Reaptcha
          ref={this.recaptchaRef}
          size='invisible'
          theme='dark'
          sitekey='6LeCyuoUAAAAAMGcmT-2fbVO2g0fQU11W5fBlkUW'
          onVerify={this.findAccount.bind(this)}
        />
        <h3>
          {enterTagTitle}
        </h3>
        <InputField onKeyUp={e => {
          if ( e.key === 'Enter' ) this.recaptchaRef.current.execute()
        }}
                    onChange={e => {
                      this.setState({ tag: e.target.value })
                    }} value={this.state.tag} type="text" placeholder="#XXXXXXXX"
                    style={{ textTransform: 'uppercase' }}
        />
        <button onClick={() => {
          this.recaptchaRef.current.execute()
        }}
                className={`round-btn${!this.validate.bind(this)() ? ' disabled' : ''}`}>
          <span>{enterTagButton}</span>
        </button>
      </>
    )
  }
}
