import React from 'react'
import ym from 'react-yandex-metrika'
import Reaptcha from 'reaptcha'
import { Link } from 'react-router-dom'
import { Redirect } from 'react-router'
import { apiRequest } from '../../../utils'
import { InputField } from '../../../includes'

export class SetLink extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      inviteLink: ''
    }

    this.recaptchaRef = React.createRef()
  }

  setLink(token) {
    const { tag } = this.props.location.state.profile
    const {
      setLoading,
      showPopup,
      history,
      texts
    } = this.props
    const {
      setLinkInvalid,
      setLinkForeign,
      setLinkWelcome,
      errorPopup,
      captchaFailedPopup
    } = texts
    setLoading(true)
    apiRequest(({ status, reason, captchaError, redirectToStage, profile }) => {
      setLoading(false)
      if ( captchaError ) {
        showPopup(captchaFailedPopup)
      } else if ( status === 'ok' && redirectToStage === 'confirm_password' ) {
        ym('reachGoal', 'accountCreation')
        showPopup(setLinkWelcome, true)
        history.push('/')
      } else if ( redirectToStage !== 'confirm_password' ) {
        history.push({
          pathname: `/login/${redirectToStage}/`,
          state: { profile: profile }
        })
      } else if ( reason ) {
        const reasons = {
          invalidLink: setLinkInvalid,
          tagsMismatch: setLinkForeign
        }
        showPopup(reasons[reason])
      } else {
        showPopup(errorPopup)
      }
    }, 'invite_link', {
      method: 'POST',
      data: {
        ...this.state,
        tag: tag,
        token: token
      }
    })
  }

  render() {
    const { inviteLink } = this.state
    if ( !this.props.location.state ) return <Redirect to='/login/enter_tag/'/>
    const {
      setLinkTitle,
      setLinkPlaceholder,
      setLinkButton,
      agreementText,
      agreementLink
    } = this.props.texts
    return (
      <>
        <Reaptcha
          ref={this.recaptchaRef}
          size='invisible'
          theme='dark'
          sitekey='6LeCyuoUAAAAAMGcmT-2fbVO2g0fQU11W5fBlkUW'
          onVerify={this.setLink.bind(this)}
        />
        <h3>
          {setLinkTitle}
        </h3>
        <InputField
          onKeyUp={e => {
            if ( e.key === 'Enter' ) this.recaptchaRef.current.execute()
          }}
          onChange={e => {
            this.setState({ inviteLink: e.target.value })
          }}
          value={inviteLink}
          type="text"
          placeholder={setLinkPlaceholder}
        />
        <button onClick={() => {
          this.recaptchaRef.current.execute()
        }}
                className={`round-btn${!inviteLink ? ' disabled' : ''}`}>
          <span>{setLinkButton}</span>
        </button>
        <div className="agreement">
          {agreementText} <Link target="_blank" rel="noopener noreferrer" to="/tos/">{agreementLink}</Link>
        </div>
      </>
    )
  }
}