import React from 'react'
import { Redirect } from 'react-router'
import { apiRequest } from '../../../utils'
import Reaptcha from 'reaptcha'

export class ConfirmAccount extends React.Component {
  constructor(props) {
    super(props)

    this.recaptchaRef = React.createRef()
  }

  checkBattle(token) {
    const { tag } = this.props.location.state.profile
    const {
      setLoading,
      showPopup,
      history,
      texts
    } = this.props
    const {
      confirmAccountWaitPopup,
      captchaFailedPopup,
      errorPopup
    } = texts
    setLoading(true)
    apiRequest(({ status, captchaError, redirectToStage, profile }) => {
      setLoading(false)
      if ( captchaError ) {
        showPopup(captchaFailedPopup)
      } else if ( status === 'ok' && redirectToStage !== 'confirm_account' ) {
        history.push({
          pathname: `/login/${redirectToStage}/`,
          state: { profile: profile }
        })
      } else if ( redirectToStage === 'confirm_account' ) {
        showPopup(confirmAccountWaitPopup)
      } else {
        showPopup(errorPopup)
      }
    }, 'confirm_account', {
      method: 'POST',
      data: {
        tag: tag,
        token: token
      }
    })
  }

  render() {
    if ( !this.props.location.state ) return <Redirect to='/login/enter_tag/'/>
    const {
      username,
      cards
    } = this.props.location.state.profile
    const {
      confirmAccountTitle,
      confirmAccountInfoTop,
      confirmAccountInfoBottom,
      confirmAccountButtonLeft,
      confirmAccountButtonRight
    } = this.props.texts
    return (
      <>
        <Reaptcha
          ref={this.recaptchaRef}
          size='invisible'
          theme='dark'
          sitekey='6LeCyuoUAAAAAMGcmT-2fbVO2g0fQU11W5fBlkUW'
          onVerify={this.checkBattle.bind(this)}
        />
        <h3 dangerouslySetInnerHTML={{ __html: confirmAccountTitle.replace('{{username}}', username) }}/>
        <div className="info">
          {confirmAccountInfoTop}
        </div>
        <div className="deck">
          {cards.map(({ iconUrls, name }, index) => (
            <div key={index} className="card">
              <img src={iconUrls.medium} alt={name}/>
            </div>
          ))}
        </div>
        <div className="info">
          {confirmAccountInfoBottom}
        </div>
        <div className="buttons">
          <a href={`clashroyale://copyDeck?deck=${cards.map(c => c.id).join(';')}`}
             target="_blank" rel="noopener noreferrer"
             className="round-btn purple">
            <span>{confirmAccountButtonLeft}</span>
          </a>
          <button onClick={() => this.recaptchaRef.current.execute()} className="round-btn">
            <span>{confirmAccountButtonRight}</span>
          </button>
        </div>
      </>
    )
  }
}