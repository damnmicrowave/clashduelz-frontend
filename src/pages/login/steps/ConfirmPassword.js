import React from 'react'
import { apiRequest } from '../../../utils'
import { Redirect } from 'react-router'
import { InputField } from '../../../includes'
import Reaptcha from 'reaptcha'

export class ConfirmPassword extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      password: ''
    }

    this.recaptchaConfirm = React.createRef()
    this.recaptchaReset = React.createRef()
  }

  confirmPassword(token) {
    const { profile } = this.props.location.state
    const {
      updateProfile,
      setLoading,
      showPopup,
      history,
      texts
    } = this.props
    const {
      incorrectPasswordPopup,
      captchaFailedPopup,
      errorPopup
    } = texts
    setLoading(true)
    apiRequest(({ status, captchaError, reason, redirectToStage, profile }) => {
      setLoading(false)
      if ( captchaError ) {
        showPopup(captchaFailedPopup)
      } else if ( status === 'ok' && redirectToStage ) {
        history.push({
          pathname: `/login/${redirectToStage}/`,
          state: { profile: profile }
        })
      } else if ( !redirectToStage && !reason ) {
        updateProfile(profile)
        history.push('/')
      } else if ( reason ) {
        showPopup(incorrectPasswordPopup)
      } else {
        showPopup(errorPopup)
      }
    }, 'confirm_password', {
      method: 'POST',
      data: {
        ...this.state,
        tag: profile.tag,
        token: token
      }
    })
  }

  resetPassword(token) {
    const {
      setLoading,
      showPopup,
      history,
      texts
    } = this.props
    const {
      notFoundPopup,
      noSuchUserPopup,
      captchaFailedPopup,
      errorPopup
    } = texts
    setLoading(true)
    apiRequest(({ error, captchaError, notFound, userNotFound, profile }) => {
      setLoading(false)
      if ( captchaError ) {
        showPopup(captchaFailedPopup)
      } else if ( notFound ) {
        showPopup(notFoundPopup)
      } else if ( userNotFound ) {
        showPopup(noSuchUserPopup)
      } else if ( !error ) {
        history.push({
          pathname: '/login/confirm_account/',
          state: { profile: profile }
        })
      } else {
        showPopup(errorPopup)
      }
    }, 'reset_password', {
      method: 'POST',
      data: {
        tag: this.props.location.state.profile.tag,
        token: token
      }
    })
  }

  render() {
    const { password } = this.state
    if ( !this.props.location.state ) return <Redirect to='/login/enter_tag/'/>
    const {
      confirmPasswordTitle,
      confirmPasswordPlaceholder,
      confirmPasswordButton,
      resetPasswordLink
    } = this.props.texts
    return (
      <>
        <Reaptcha
          ref={this.recaptchaReset}
          size='invisible'
          theme='dark'
          sitekey='6LeCyuoUAAAAAMGcmT-2fbVO2g0fQU11W5fBlkUW'
          onVerify={this.resetPassword.bind(this)}
        />
        <Reaptcha
          ref={this.recaptchaConfirm}
          size='invisible'
          theme='dark'
          sitekey='6LeCyuoUAAAAAMGcmT-2fbVO2g0fQU11W5fBlkUW'
          onVerify={this.confirmPassword.bind(this)}
        />
        <h3>
          {confirmPasswordTitle}
        </h3>
        <InputField onKeyUp={e => {
          if ( e.key === 'Enter' ) this.recaptchaConfirm.current.execute()
        }}
                    onChange={e => {
                      this.setState({ password: e.target.value })
                    }}
                    value={password}
                    type="password" placeholder={confirmPasswordPlaceholder}/>
        <button onClick={() => {
          this.recaptchaConfirm.current.execute()
        }}
                className={`round-btn${password.length >= 8 ? '' : ' disabled'}`}>
          <span>{confirmPasswordButton}</span>
        </button>
        <button onClick={() => {
          this.recaptchaReset.current.execute()
        }} className="link">{resetPasswordLink}</button>
      </>
    )
  }
}
