import React from 'react'
import { NavLink } from 'react-router-dom'
import { apiRequest, getParents } from '../../utils'
import './styles.scss'
import zCoin from './media/zCoin.svg'
import plusSVG from './media/plus.svg'
import timesSVG from './media/times.svg'
import menuSVG from './media/menu.svg'
import zSVG from './media/z.svg'
import clashduelSVG from './media/clashduel.svg'

class LanguagePicker extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      expanded: false
    }

    this.list = React.createRef()
  }

  componentDidMount() {
    window.addEventListener('click', e => {
      if ( !getParents(e.target).includes(this.list.current) ) this.setState({ expanded: false })
    })
  }

  selectLang(lang) {
    this.props.selectLang(lang)
    this.setState({ expanded: false })
  }

  render() {
    const {
      lang
    } = this.props
    return (
      <div ref={this.list} className={`language${this.state.expanded ? ' expanded' : ''}`}>
        <span onClick={() => {
          this.setState({ expanded: true })
        }}>{lang}</span>
        <div className="variants">
          <div className={lang === 'EN' ? 'active' : ''} onClick={() => {
            this.selectLang.bind(this)('EN')
          }}>EN
          </div>
          <div className={lang === 'RU' ? 'active' : ''} onClick={() => {
            this.selectLang.bind(this)('RU')
          }}>RU
          </div>
        </div>
      </div>
    )
  }
}

export class NavBar extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      sliderToggled: false,
      showNotification: true
    }

    this.slider = React.createRef()
    this.sliderToggler = React.createRef()
  }

  componentDidMount() {
    window.addEventListener('click', e => {
      const parents = getParents(e.target)
      if (
        !parents.includes(this.slider.current) &&
        !parents.includes(this.sliderToggler.current) &&
        this.state.sliderToggled
      ) {
        this.toggleSlider()
      }
    })
  }

  hideNotification() {
    const {
      loggedIn,
      updateProfile
    } = this.props
    if ( loggedIn ) {
      apiRequest(() => {
        updateProfile({ 'seen_notification': true })
      }, 'close_notification')
    } else {
      this.setState({ showNotification: false })
    }
  }

  toggleSlider() {
    this.setState({ sliderToggled: !this.state.sliderToggled })
  }

  render() {
    let {
      sliderToggled,
      showNotification
    } = this.state
    const {
      loggedIn,
      profile,
      notification,
      lang,
      texts
    } = this.props
    const {
      tag,
      balance
    } = profile
    const {
      duels,
      leaderboard,
      faq,
      tos,
      support,
      login,
      balance: balanceText
    } = texts
    const NavLinkMobile = props => (
      <NavLink onClick={this.toggleSlider.bind(this)} {...props}/>
    )
    showNotification = loggedIn ? !profile.seen_notification : showNotification
    return (
      <div className='navbar'>
        <div className="wrapper">
          <NavLink to="/" className="logo-wrapper">
            <div className="logo">
              <img src={clashduelSVG} alt="logo"/>
              <img src={zSVG} alt="logo"/>
            </div>
          </NavLink>
          <div className="links">
            <NavLink exact to="/">{duels}</NavLink>
            <NavLink exact to="/leaderboard/">{leaderboard}</NavLink>
            <NavLink exact to="/faq/">{faq}</NavLink>
            <NavLink exact to="/tos/">{tos}</NavLink>
            <NavLink exact to="/support/">{support}</NavLink>
          </div>
          <div className="profile-info">
            {loggedIn ?
              <NavLink to="/profile/" className="tag" style={{ textTransform: 'uppercase' }}>#{tag}</NavLink> :
              <NavLink to="/login/" className="tag">{login}</NavLink>
            }
            <NavLink to="/balance/" className="balance">
              {!balance && <img src={plusSVG} alt="balance"/>}
              <span>{loggedIn ? balance : 0}</span>
              <img src={zCoin} alt="z coin"/>
            </NavLink>
            <LanguagePicker {...this.props}/>
          </div>
          <span ref={this.sliderToggler} onClick={this.toggleSlider.bind(this)} className="menu-toggler">
                        <img src={menuSVG} alt="open menu"/>
                    </span>
        </div>
        {notification && showNotification && !profile.seen_notification && <div className="notification">
          <span className="text">{notification[lang]}</span>
          <div onClick={this.hideNotification.bind(this)} className="img">
            <img src={plusSVG} alt="close notification"/>
          </div>
        </div>}
        <div ref={this.slider} className={`slider${sliderToggled ? ' active' : ''}`}>
                    <span onClick={this.toggleSlider.bind(this)} className="close-menu">
                        <img src={timesSVG} alt="close menu"/>
                    </span>
          <div className="links">
            <NavLinkMobile exact to="/">{duels}</NavLinkMobile>
            <NavLinkMobile exact to="/leaderboard/">{leaderboard}</NavLinkMobile>
            <NavLinkMobile exact to="/faq/">{faq}</NavLinkMobile>
            <NavLinkMobile exact to="/tos/">{tos}</NavLinkMobile>
            <NavLinkMobile exact to="/support/">{support}</NavLinkMobile>
            <div className="profile-info">
              {loggedIn ?
                <NavLinkMobile to="/profile/" className="tag"
                               style={{ textTransform: 'uppercase' }}>#{tag}</NavLinkMobile> :
                <NavLinkMobile to="/login/" className="tag">{login}</NavLinkMobile>
              }
              <NavLinkMobile to="/balance/" className="balance">
                <span>{balanceText}: {loggedIn ? balance : 0}</span>
                <img src={zCoin} alt="z coin"/>
              </NavLinkMobile>
              <LanguagePicker {...this.props}/>
            </div>
          </div>
        </div>
      </div>
    )
  }
}