import React from 'react'
import Reaptcha from 'reaptcha'
import ym from 'react-yandex-metrika'
import { InputField } from '../../includes'
import { apiRequest } from '../../utils'
import './styles.scss'

export class Support extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      name: '',
      email: '',
      message: ''
    }
    this.recaptchaRef = React.createRef()
  }

  contactSupport(recaptchaResponse) {
    const {
      setLoading,
      showPopup,
      texts
    } = this.props
    setLoading(true)
    apiRequest(({ captchaError }) => {
      setLoading(false)
      if ( !captchaError ) {
        ym('reachGoal', 'inTrouble')
        showPopup(texts.popup)
      } else {
        showPopup(texts.captchaFailedPopup)
      }
    }, 'contact_support', { method: 'POST', data: { ...this.state, token: recaptchaResponse } })
  }

  validate() {
    const {
      name,
      email,
      message
    } = this.state
    const emailRegex = /(?!^[.+&'_-]*@.*$)(^[_\w\d+&'-]+(\.[_\w\d+&'-]*)*@[\w\d-]+(\.[\w\d-]+)*\.(([\d]{1,3})|([\w]{2,}))$)/
    return name && message && emailRegex.test(email.trim())
  }

  render() {
    const {
      name,
      email,
      message
    } = this.state
    const {
      title,
      namePlaceholder,
      emailPlaceholder,
      messagePlaceholder,
      button,
      bottomText
    } = this.props.texts
    return (
      <div className="support container">
        <h3 dangerouslySetInnerHTML={{ __html: title }}/>
        <div className="form">
          <Reaptcha
            ref={this.recaptchaRef}
            size='invisible'
            theme='dark'
            sitekey='6LeCyuoUAAAAAMGcmT-2fbVO2g0fQU11W5fBlkUW'
            onVerify={this.contactSupport.bind(this)}
          />
          <InputField
            onKeyUp={e => {
              if ( e.key === 'Enter' ) this.contactSupport.bind(this)()
            }}
            onChange={e => {
              this.setState({ name: e.target.value })
            }}
            value={name}
            type="text" placeholder={namePlaceholder}/>
          <InputField
            onKeyUp={e => {
              if ( e.key === 'Enter' ) this.contactSupport.bind(this)()
            }}
            onChange={e => {
              this.setState({ email: e.target.value })
            }}
            value={email}
            type="text" placeholder={emailPlaceholder}/>
          <InputField
            textarea
            onChange={e => {
              this.setState({ message: e.target.value })
            }}
            value={message}
            spellCheck={false} rows="4" placeholder={messagePlaceholder}/>
          <button onClick={() => {
            this.recaptchaRef.current.execute()
          }}
                  className={`round-btn${!this.validate.bind(this)() ? ' disabled' : ''}`}>
            <span>{button}</span>
          </button>
          <div className="info">{bottomText} <a
            href="mailto:support@clashduelz.com">support@clashduelz.com</a></div>
        </div>
      </div>
    )
  }
}