const getCookie = name => {
  let cookieValue = null
  if ( document.cookie && document.cookie !== '' ) {
    const cookies = document.cookie.split(';')
    for ( let i = 0; i < cookies.length; i++ ) {
      const cookie = cookies[i].trim()
      if ( cookie.substring(0, name.length + 1) === (name + '=') ) {
        cookieValue = decodeURIComponent(cookie.substring(name.length + 1))
        break
      }
    }
  }
  return cookieValue
}

export const getParents = (node, parents = [ node ]) => {
  try {
    const parent = node.parentNode
    if ( parent ) {
      return getParents(node.parentNode, [ ...parents, parent ])
    } else {
      return parents
    }
  } catch ( e ) {
    return []
  }
}

export function apiRequest(
  func,
  apiMethod,
  {
    method = 'GET',
    data = {}
  } = {}
) {
  const xhttp = new XMLHttpRequest()
  xhttp.onreadystatechange = function () {
    if ( this.readyState === 4 && this.status === 200 ) {
      const response = JSON.parse(this.responseText)
      func(response)
    } else if ( this.status === 403 ) {
      alert('This action is forbidden. The page will reload now')
      window.location.reload()
    } else if ( this.status === 420 ) {
      alert('Too many attempts. Please try again later')
    }
  }
  const { protocol, host, hostname } = window.location
  const url = `${protocol}//${protocol === 'http:' ? hostname : host}${protocol === 'http:' ? ':8000' : ''}/api/${apiMethod}/`
  xhttp.open(method, url, true)
  if ( method === 'POST' ) {
    xhttp.setRequestHeader('Content-Type', 'application/json')
    xhttp.setRequestHeader('X-CSRFToken', getCookie('csrftoken'))
    xhttp.send(JSON.stringify(data))
  } else {
    xhttp.send()
  }
}