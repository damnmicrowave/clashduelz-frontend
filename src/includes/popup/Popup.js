import React from 'react'
import './styles.scss'

export const Popup = props => (
  <div className="popup">
    <div className="card">
      <span dangerouslySetInnerHTML={{ __html: props.message }}/>
    </div>
  </div>
)
