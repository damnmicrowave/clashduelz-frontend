import React from 'react'
import './styles.scss'

const removeProp = (props, prop) => {
  const newProps = Object.assign({}, props)
  delete newProps[prop]
  return newProps
}

export const InputField = props => (
  <div className="input-field">
    {props.textarea ? <textarea {...removeProp(props, 'textarea')} /> : <input {...props} />}
    <div className="border"/>
  </div>
)