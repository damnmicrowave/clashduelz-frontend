import React from 'react'
import { apiRequest } from '../../utils'
import './styles.scss'

export class Waiting extends React.Component {
  cancelDuel() {
    const {
      texts,
      showPopup,
      setLoading,
      updateProfile
    } = this.props
    const {
      notAuthorizedCancelErrorPopup,
      notWaitingPopup
    } = texts
    setLoading(true)
    apiRequest(({ status, reason, balance }) => {
      setLoading(false)
      const reasons = {
        notWaiting: [ notWaitingPopup, true ],
        notAuthorized: [ notAuthorizedCancelErrorPopup ]
      }
      if ( status === 'error' ) {
        showPopup(...reasons[reason])
      } else {
        updateProfile({ status: '', balance: balance })
      }
    }, 'cancel_duel')
  }

  render() {
    const style = { background: this.props.color }
    const {
      waitingTitle,
      cancelDuelButton
    } = this.props.texts
    return (
      <div className="waiting">
        <div className="waiting-box">
          <h4>{waitingTitle}</h4>
          <div className="anim-box">
            {[ ...Array(3).keys() ].map((_, index) => (
              <div key={index} style={style} className="circle"/>
            ))}
          </div>
          <button onClick={this.cancelDuel.bind(this)} className="round-btn">
            <span>{cancelDuelButton}</span>
          </button>
        </div>
      </div>
    )
  }
}
