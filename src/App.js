import React from 'react'
import { Redirect, Route as DefaultRoute, Router, Switch } from 'react-router'
import { YMInitializer } from 'react-yandex-metrika'
import { createBrowserHistory } from 'history'
import { Loading, Popup, Waiting } from './includes'
import { apiRequest } from './utils'
import {
  NavBar,
  ClanNavbar,
  ClanRules,
  ClanRating,
  ClanRecoveryTeam,
  FAQ,
  Support,
  LeaderBoard,
  Profile,
  Duels,
  Duel,
  Balance,
  Login,
  ToS,
  ErrorPage
} from './pages'
import './static/common.scss'

const queryString = require('query-string')

export class App extends React.Component {
  constructor(props) {
    super(props)

    if ( window.location.hostname.split('.')[0] === 'clan' && !('lang' in window.localStorage) ) {
      window.localStorage.setItem('lang', 'RU')
    }

    const { r: referralCode } = queryString.parse(window.location.search)
    if ( referralCode ) {
      window.localStorage.setItem('referralCode', referralCode)
      window.location.replace('/')
    }

    this.state = {
      texts: false,
      lang: 'lang' in window.localStorage ? window.localStorage.lang : 'EN',
      loggedIn: false,
      loading: true,
      showPopup: false,
      popupMessage: '',
      notification: '',
      profile: {},
      online: {
        general: 0,
        5: 0,
        25: 0,
        50: 0,
        100: 0,
        250: 0,
        500: 0
      },
      currentRoom: false
    }
    this.history = createBrowserHistory()
    this.roomSocket = false
  }

  componentDidMount() {
    const host = window.location.protocol === 'http:' ? `${window.location.hostname}:8000` : window.location.host
    this.generalSocket = new WebSocket(`${window.location.protocol === 'http:' ? 'ws:' : 'wss:'}//${host}/ws/duels/general/`)
    this.generalSocket.onmessage = this.websocketMessage.bind(this)
    apiRequest(({ loggedIn, profile, currentRoom, notification, texts }) => {
      this.setState({
        loggedIn: loggedIn,
        profile: profile,
        texts: texts,
        notification: notification && !profile.seen_notification ? JSON.parse(notification) : false,
        loading: false
      }, () => {
        if ( currentRoom ) this.enterRoom(currentRoom)
      })
    }, 'initialize')
  }

  setLoading(value) {
    this.setState({ loading: value })
  }

  updateProfile(obj) {
    this.setState({ loggedIn: true, profile: { ...this.state.profile, ...obj } })
  }

  selectLang(lang) {
    window.localStorage.lang = lang
    this.setState({ lang: lang })
  }

  showPopup(message, refresh = false) {
    this.setState({ showPopup: true, popupMessage: message })
    setTimeout(() => {
      this.setState({ showPopup: false }, () => {
        if ( refresh ) window.location.reload()
      })
    }, 2500)
  }

  websocketMessage(e) {
    const { data } = JSON.parse(e.data)
    if ( this.state.profile.status !== 'in duel' || data.message === 'NOTIFICATION' ) {
      switch ( data.message ) {
        case 'NOW_ONLINE':
          if ( window.location.pathname === '/' ) this.setState({ online: data.online })
          break
        case 'PING':
          this.generalSocket.send(
            JSON.stringify({ 'message': 'PING' })
          )
          break
        case 'DUEL_PING':
          this.roomSocket.send(
            JSON.stringify({ 'message': 'DUEL_PING' })
          )
          break
        case 'OPPONENT_FOUND':
          this.updateProfile({ status: 'in duel' })
          break
        case 'NOTIFICATION':
          this.setState({
            notification: JSON.parse(data.notification),
            profile: { ...this.state.profile, 'seen_notification': false }
          })
          break
        default:
          break
      }
    }
  }

  enterRoom(room) {
    if ( !this.state.loggedIn ) {
      this.history.push('/login/')
    } else {
      this.setState({ loading: true })
      if ( this.roomSocket ) this.roomSocket.close()
      const host = window.location.protocol === 'http:' ? `${window.location.hostname}:8000` : window.location.host
      this.roomSocket = new WebSocket(`${window.location.protocol === 'http:' ? 'ws:' : 'wss:'}//${host}/ws/duels/${room}/`)
      this.roomSocket.onmessage = this.websocketMessage.bind(this)
      this.roomSocket.onopen = () => {
        this.setState({ currentRoom: room, loading: false })
      }
      this.roomSocket.onerror = () => {
        this.setState({ loading: false })
        this.showPopup(this.state.texts[this.state.lang].duels.errorJoiningPopup)
      }
    }
  }

  render() {
    const {
      texts,
      lang,
      loading,
      showPopup,
      popupMessage,
      loggedIn,
      profile,
      online,
      notification,
      currentRoom
    } = this.state
    const {
      header,
      duels,
      faq,
      tos,
      leaderboard,
      support,
      login,
      profile: profileTexts,
      balance,
      clanHeader,
      clanRules,
      clanRating,
      clanRecoveryTeam
    } = texts ? texts[lang] : {}
    const setPopup = this.showPopup.bind(this)
    const setLoading = this.setLoading.bind(this)
    const updateProfile = this.updateProfile.bind(this)
    const Route = props => (
      <DefaultRoute {...props} render={routeProps => (
        profile.status !== 'in duel' || props.path === '/support/'
          ? props.render(routeProps)
          : <Duel
            loading={loading}
            texts={duels}
            profile={profile}
            roomSocket={this.roomSocket}
            setLoading={setLoading}
            showPopup={setPopup}
            updateProfile={updateProfile}
          />
      )}/>
    )
    return (

      <YMInitializer accounts={[ 61629922 ]}
                     options={{
                       clickmap: true,
                       trackLinks: true,
                       accurateTrackBounce: true,
                       webvisor: true
                     }} version="2">
        {loading && <Loading/>}
        {!loading && profile.status === 'waiting' &&
        <Waiting
          texts={duels}
          updateProfile={updateProfile}
          setLoading={setLoading}
          showPopup={setPopup}
        />}
        {showPopup && <Popup message={popupMessage}/>}
        {texts &&
        <div className={`root${showPopup || loading || profile.status === 'waiting' ? ' blurred' : ''}`}>
          {window.location.hostname.split('.')[0] !== 'clan' ?
            <Router history={this.history}>
              <NavBar
                selectLang={this.selectLang.bind(this)}
                lang={lang}
                profile={profile}
                updateProfile={updateProfile}
                loggedIn={loggedIn}
                texts={header}
                notification={notification}
              />
              <Switch>
                <Route exact path="/" render={() =>
                  <Duels
                    texts={duels}
                    online={online}
                    history={this.history}
                    currentRoom={currentRoom}
                    enterRoom={this.enterRoom.bind(this)}
                    updateProfile={updateProfile}
                    setLoading={setLoading}
                    showPopup={setPopup}
                  />
                }/>
                <Route exact path="/payment_success/" render={() => {
                  setPopup(balance.paymentSuccessfulPopup)
                  return <Redirect to="/"/>
                }}/>
                <Route exact path="/payment_fail/" render={() => {
                  setPopup(balance.paymentFailedPopup)
                  return <Redirect to="/"/>
                }}/>
                <Route exact path="/faq/" render={() => <FAQ texts={faq}/>}/>
                <Route exact path="/tos/" render={() => <ToS texts={tos}/>}/>
                <Route exact path="/support/" render={() =>
                  <Support
                    texts={support}
                    setLoading={setLoading}
                    showPopup={setPopup}
                  />
                }/>
                <Route exact path="/leaderboard/" render={() =>
                  <LeaderBoard texts={leaderboard}/>
                }/>
                <Route exact path="/profile/" render={() =>
                  <Profile
                    setLoading={setLoading}
                    showPopup={setPopup}
                    loggedIn={loggedIn}
                    profile={profile}
                    updateProfile={updateProfile}
                    texts={profileTexts}
                  />
                }/>
                <Route path="/balance/"
                       render={() =>
                         <Balance
                           loggedIn={loggedIn}
                           texts={balance}
                           showPopup={setPopup}
                           setLoading={setLoading}
                           updateProfile={updateProfile}
                           history={this.history}
                           balance={profile.balance}
                         />
                       }/>
                <Route exact path={[ '/login/:step/', '/login/' ]} render={routeProps =>
                  <Login
                    {...routeProps}
                    texts={login}
                    history={this.history}
                    showPopup={setPopup}
                    updateProfile={updateProfile}
                    setLoading={setLoading}
                    loggedIn={loggedIn}
                  />
                }/>
                <Route path="/" render={() => <ErrorPage/>}/>
              </Switch>
            </Router> :
            <Router history={this.history}>
              <ClanNavbar
                selectLang={this.selectLang.bind(this)}
                lang={lang}
                texts={clanHeader}
              />
              <Switch>
                <Route exact path="/" render={() => <ClanRules texts={clanRules}/>}/>
                <Route exact path="/rating/" render={() =>
                  <ClanRating texts={clanRating} loggedIn={loggedIn}/>
                }/>
                <Route exact path="/cw_info/" render={() => <ToS texts={tos}/>}/>
                <Route exact path="/recovery_team/" render={() => <ClanRecoveryTeam texts={clanRecoveryTeam}/>}/>
                <Route path="/" render={() => <ErrorPage/>}/>
              </Switch>
            </Router>
          }
        </div>}
      </YMInitializer>
    )
  }
}
